#include "interrupt_handler_utility.h"
#include "LPC11XX.h"

int TickCount = 0;
void SysTick_Handler(void)
{
	TickCount++;
}

void delay_ms(int ms)
{
	int temp = TickCount + ms;
	while (temp > TickCount) { __WFI(); };
}

