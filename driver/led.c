#include "LPC11XX.h"
#include "led.h"

void LedOutputCfg(void) {
	LPC_SYSCON->SYSAHBCLKCTRL = LPC_SYSCON->SYSAHBCLKCTRL | (1<<16) | (1<<6);
	
	LPC_IOCON->PIO0_7 = (0x0) + (0<<3) + (0<<5);
	LPC_GPIO0->DIR = LPC_GPIO0->DIR | (1<<7);
	LPC_GPIO0->MASKED_ACCESS[1<<7] = (0<<7);
	
	return;
}

void LedOn(void) {
	LPC_GPIO0->MASKED_ACCESS[1<<7] = (1<<7);
}

void LedOff(void) {
	LPC_GPIO0->MASKED_ACCESS[1<<7] = (0<<7);
}

void LefToggle(void) {
	LPC_GPIO0->MASKED_ACCESS[1<<7] = ~LPC_GPIO0->MASKED_ACCESS[1<<7];
}
