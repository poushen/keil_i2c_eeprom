#ifndef UART_H
#define UART_H

// Function declarations
void          UartConfig(void);         // Uart configuration
unsigned char UartPutc(unsigned char my_ch);     // Uart character output
void          UartPuts(unsigned char * mytext);  // Uart string output
int           UartGetRxDataAvail(void); // Detect new data are received
unsigned char UartGetRxData(void);      // Get received data from Uart
unsigned char UartGetc(void);           // Uart character input

#endif
